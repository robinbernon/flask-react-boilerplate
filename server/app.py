from flask import Flask, send_from_directory
from flask_cors import CORS
import json
import flask
import os.path
import argparse
import datetime

STATIC_FOLDER_PATH = f"{os.path.abspath('')}\\build\\"
REACT_SITES = [] # Fill with paths for react sites

TESTING_PORT_NUMBER = 5000
PRODUCTION_PORT_NUMBER = 5000

app = Flask(__name__, static_folder=STATIC_FOLDER_PATH)
CORS(app)

##
# API routes
##


@app.route("/api/ping")
def health_check():
    return "Response"

@app.route('/api/time')
def get_current_time():
    current_time = datetime.datetime.now()
    return json.dumps({'time': f"{current_time.hour}:{current_time.minute}:{current_time.second}"})

##
# View route
##


@app.errorhandler(404)
def not_found(error):
    return json.dumps({"error":str(error)})

def get_not_found_error_response():
    return json.dumps({"error":"404 Not Found: The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."})

@app.route("/", defaults={"path": "index.html"})
@app.route("/<path>")
def index(path):
    print(f"path = {path}")

    if path in REACT_SITES:
        return send_from_directory(app.static_folder, "index.html")

    if os.path.exists(f"{STATIC_FOLDER_PATH}{path}"):
        return send_from_directory(app.static_folder, path)

    return flask.make_response(get_not_found_error_response(), 404)

##
# Argparsing
##

def _str_to_bool(s):
    """Convert string to bool (in argparse context)."""
    if s.lower() not in ['true', 'false']:
        raise ValueError('Need bool; got %r' % s)
    return {'true': True, 'false': False}[s.lower()]

def add_boolean_argument(parser, name, default=False):                                                                                               
    """Add a boolean argument to an ArgumentParser instance."""
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--' + name, nargs='?', default=default, const=True, type=_str_to_bool)
    group.add_argument('--no' + name, dest=name, action='store_false')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    add_boolean_argument(parser, 'debug', default=False)
    args = parser.parse_args()

    if args.debug:
        app.run(debug=True, host="0.0.0.0", port=TESTING_PORT_NUMBER)
    else:
        app.run(debug=False, host="0.0.0.0", port=PRODUCTION_PORT_NUMBER)